import java.util.*;

public class TxHandler {

    private UTXOPool utxoPool;

    /**
     * Creates a public ledger whose current UTXOPool (collection of unspent transaction outputs) is
     * {@code utxoPool}. This should make a copy of utxoPool by using the UTXOPool(UTXOPool uPool)
     * constructor.
     */
    public TxHandler(UTXOPool utxoPool) {
        this.utxoPool = new UTXOPool(utxoPool);
    }

    public String getHashContents(byte[] hash) {
        return Arrays.toString(hash);
    }

    /**
     * @return true if:
     * (1) all outputs claimed by {@code tx} are in the current UTXO pool, 
     * (2) the signatures on each input of {@code tx} are valid, 
     * (3) no UTXO is claimed multiple times by {@code tx},
     * (4) all of {@code tx}s output values are non-negative, and
     * (5) the sum of {@code tx}s input values is greater than or equal to the sum of its output
     *     values; and false otherwise.
     */
    public boolean isValidTx(Transaction tx) {
        float iSum = 0;
        float oSum = 0;

        System.out.println("Pool contents: ");
        for (UTXO u : this.utxoPool.getAllUTXO()) {
            System.out.println("utxo: " + u.getIndex() + ", " + this.getHashContents(u.getTxHash()));
        }

        ArrayList<Transaction.Input> inputs = tx.getInputs();
        System.out.println("Tx inputs: ");
        for (Transaction.Input input : inputs) {
//            int i = java.util.Arrays.asList(inputs).indexOf(input);
//            System.out.println("input: " + input + ", " + input.prevTxHash + ", " + input.outputIndex);
            System.out.println("input: " + input.outputIndex + ", " + this.getHashContents(input.prevTxHash));
            UTXO utxo = new UTXO(input.prevTxHash, input.outputIndex);
            if (!this.utxoPool.contains(utxo)) {
                return false;
            }

            if (!Crypto.verifySignature(
                    utxoPool.getTxOutput(utxo).address,
                    tx.getRawDataToSign(inputs.indexOf(input)),
                    input.signature
            )) {
                return false;
            }

            iSum += utxoPool.getTxOutput(utxo).value;
        }


        List<UTXO> list = utxoPool.getAllUTXO();
        Set<UTXO> set = new HashSet<UTXO>(list);

        if (set.size() < list.size()){
            return false;
        }

        ArrayList<Transaction.Output> outputs = tx.getOutputs();
        for (Transaction.Output output : outputs) {
            if (output.value < 0) {
                return false;
            }

            oSum += output.value;
        }

        if (iSum < oSum) {
            return false;
        }

        return true;
    }

    /**
     * Handles each epoch by receiving an unordered array of proposed transactions, checking each
     * transaction for correctness, returning a mutually valid array of accepted transactions, and
     * updating the current UTXO pool as appropriate.
     */
    public Transaction[] handleTxs(Transaction[] possibleTxs) {
        ArrayList<Transaction> res = new ArrayList<Transaction>();

//        foreach tx
//                if isValidTx(tx)
//        foreachInput
//        this.utxoPool.removeUTXO();
//
//        for outputs
//                this.utxoPool.addUTXO(output);
//        res.push(tx)

        for(Transaction tx : possibleTxs) {
            if (isValidTx(tx)) {
                for (Transaction.Input i : tx.getInputs()) {
                    UTXO utxo = new UTXO(i.prevTxHash, i.outputIndex);
                    this.utxoPool.removeUTXO(utxo);
                }

                for (Transaction.Output o : tx.getOutputs()) {
                    UTXO utxo = new UTXO(tx.getHash(), tx.getOutputs().indexOf(o));
                    this.utxoPool.addUTXO(utxo, o);
                }

                res.add(tx);
            }
        }


        System.out.println("Pool contents: ");
        for (UTXO u : this.utxoPool.getAllUTXO()) {
            System.out.println("utxo: " + u.getIndex() + ", " + this.getHashContents(u.getTxHash()));
        }

        return res.toArray(new Transaction[0]);
    }

}